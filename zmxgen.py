#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  9 13:03:26 2019

@author: chazelas
"""

import numpy as n

header="""VERS 110711 289 27216
MODE SEQ
NAME 
UNIT MM X W X CM MR CPMM
ENPD 1.0E+2
ENVD 2.0E+1 1 0
GFAC 0 0
GCAT SCHOTT MISC
RAIM 0 0 1 1 0 0 0 0 0
PUSH 0 0 0 0 0 0
SDMA 0 1 0
ROPD 2
PICB 1
"""

def array_string(a):
   return " ".join([str(s) for s in a]) 


class surface():
    def __init__(self,curvature,conic,thickness,glass=None,comment="",decenterx=0,decentery=0,reverse=False,stop=False,aperturetype=None,**kwargs):
        self.curvature = curvature
        self.thickness = thickness
        self.glass = glass
        self.decentery = decentery
        self.decenterx = decenterx
        self.conic = conic
        self.aperturetype = aperturetype
        self.comment = comment
        self.reverse = reverse
        if aperturetype != None:
            if aperturetype == "circular":
                self.radius = kwargs["radius"]
            if aperturetype == "rectangular":
                self.xsize = kwargs["xsize"]
                self.ysize = kwargs["ysize"]
            if "adecenterx" in kwargs:
                self.adecenterx = kwargs["adecenterx"]
                self.adecentery = kwargs["adecentery"]
        self.stop = stop
        if "xtangent" in kwargs:
            self.xtangent = kwargs["xtangent"]
        else:
            self.xtangent = 0
        if "ytangent" in kwargs:
            self.ytangent = kwargs["ytangent"]
        else:
            self.ytangent = 0

        #print("surface : ",vars(self))



cbrk = {"decenterx":1,"decentery":2,"tiltx":3,"tilty":4,"tiltz":5,"order":6}        
class coordbreak():
    def __init__(self,**kwargs):
        
        if set(kwargs.keys()).issubset(["decenterx","decentery","tiltx","tilty","tiltz","order"]):
            for k in kwargs.keys():
                setattr(self,k,kwargs[k])
        else:
            raise Exception('One parameter is spelled wrong')
            

class optical_model():
    def __init__(self):
        self.buffer = header
        self.surfn = 0
        self.nfields = 1
        self.nwave = 1
        
    def fields(self,fields=n.array([[0,0]])):
        self.buffer += "XFLN {fieldsx}\n".format(fieldsx = array_string(fields[:,0]))
        self.buffer += "YFLN {fieldsy}\n".format(fieldsy = array_string(fields[:,1]))
        self.buffer += "FWGN {fieldswgt}\n".format(fieldswgt =array_string(n.ones(fields[:,0].shape)))
        self.nfields=fields.shape[0]
    def wavelength_flat(self,wavelength):
        self.buffer += "WAVL {wave}\n".format(wave =array_string(wavelength))
        self.buffer += "WWGT {wgt}\n".format(wgt=array_string(n.ones(wavelength.shape)))
        self.buffer += "PWAV 1\n"
        self.nwave=wavelength.shape[0]
    def nconfigs(self,n_configs):
        self.buffer+="MNUM {n_config}\n".format(n_config=n_configs)
        
    def wave_config(self,wavelengths):
        for k in range(wavelengths.shape[1]):
            for i in range(wavelengths.shape[0]):
                self.buffer+="WAVE  {confign} {waven} {wavelength} 0 0 0 1 1 1.000000000000E+000 0.000000000000E+000\n".format(confign=k+1,waven=i+1,wavelength=wavelengths[i,k])
    
    def param_config(self,surf,n,values):
        for k in range(values.shape[0]):
            self.buffer+="PRAM   {} {} {} 0 {} 0 1 1 1.000000000000E+000 0.000000000000E+000 0\n".format(surf,k+1,values[k],n)
            
    def ENP_diameter(self,d,telecentric=False):
        self.buffer += "ENPD {}\n".format(d)
        self.buffer += "PUPD 0 {}\n".format(d)
        self.buffer += "GFAC 0 0\n"
    
    def NA_aperture(self,na,telecentric=False):
        self.buffer += "OBNA {} 0\n".format(na)
        if telecentric:
            tel = 1e10
            d = n.tan(n.arcsin(na)) * tel*2
            self.buffer += "FTYP  1 1 {} {} 0 0 0\n".format(self.nfields,self.nwave)
        else:
            tel = 0
            d = 0
            self.buffer += "FTYP  1 0 {} {} 0 0 0\n".format(self.nfields,self.nwave)
            
        self.buffer += "PUPD {telecentric} {d}\n".format(telecentric=tel,d=d)
        self.buffer += "GFAC 0 0\n"
    
    
    
    def standard_surface(self,surface):
        self.buffer += "SURF {}\n".format(self.surfn)
        self.surfn  += 1
        self.buffer += "\tTYPE STANDARD\n"
        self.buffer += "\tCURV {} 0 0.000000000000E+000 0.000000000000E+000\n".format(surface.curvature)
        self.buffer += "\tSLAB {}\n".format(self.surfn)
        self.buffer += "\tSCBD 1 0 0 {decenterx} {decentery} 0.000000000000E+000 0.000000000000E+000 0.000000000000E+000\n".format(decenterx=surface.decenterx,decentery=surface.decentery)
        if surface.reverse:
            self.buffer += "\tSCBD 2 1 2 {decenterx} {decentery} 0.000000000000E+000 0.000000000000E+000 0.000000000000E+000\n".format(decenterx=-surface.decenterx,decentery=-surface.decentery)
        self.buffer += "\tCOMM {}\n".format(surface.comment)
        if isinstance(surface.thickness,str):
            if surface.thickness[0]=="P":
                self.buffer += "\tDISZ {}\n".format(0)
                self.buffer += "\tPZUP {}\n".format(surface.thickness[1:])
            if surface.thickness[0]=="M":
                self.buffer += "\tDISZ {}\n".format(0)
                self.buffer += "\tMAZH {} {}\n".format(0,1)
            else:
                self.buffer += "\tDISZ {}\n".format(surface.thickness)
        else:
            self.buffer += "\tDISZ {}\n".format(surface.thickness)
        
        self.buffer += "\tCONI {}\n".format(surface.conic)
        self.buffer += "\tDIAM 0.000000000000E+000 0 0\n"
        if surface.glass!=None:
            self.buffer += "\tGLAS {} 0 0 1.50000000 40.00000000 0.00000000 0 0 0 0.00000000 0.00000000\n".format(surface.glass)
        self.buffer += "\tPOPS 0 0 0 0 0 0 0 0 1 1 1.000000000000E+000 1.000000000000E+000\n"
        if surface.stop:
            self.buffer += "\tSTOP\n"
        if surface.aperturetype =="circular":
            self.buffer+="\tCLAP 0.000000000000E+000 {}\n".format(surface.radius)
        if surface.aperturetype =="rectangular":
            self.buffer+="\tSQAP {} {}\n".format(surface.xsize,surface.ysize)
        if hasattr(surface,"adecenterx"):
            self.buffer+="\tOBDC {} {}\n".format(surface.adecenterx,surface.adecentery)

    def tilted_surface(self, surface):
        self.buffer += "SURF {}\n".format(self.surfn)
        self.surfn += 1
        self.buffer += "\tTYPE TILTSURF\n"
        self.buffer += "\tCURV {} 0 0.000000000000E+000 0.000000000000E+000\n".format(surface.curvature)
        self.buffer += "\tSLAB {}\n".format(self.surfn)
        self.buffer += "\tSCBD 1 0 0 {decenterx} {decentery} 0.000000000000E+000 0.000000000000E+000 0.000000000000E+000\n".format(
            decenterx=surface.decenterx, decentery=surface.decentery)
        if isinstance(surface.xtangent,str):
            self.buffer += f"\tPARM 1 0\n"
            self.buffer += f"\tPPAR 1 {surface.xtangent[1:]}\n"
        else:
            self.buffer += f"\tPARM 1 {surface.xtangent}\n"

        if isinstance(surface.ytangent, str):
            self.buffer += f"\tPARM 2 0\n"
            self.buffer += f"\tPPAR 2 {surface.ytangent[1:]}\n"
        else:
            self.buffer += f"\tPARM 2 {surface.ytangent}\n"
        if surface.reverse:
            self.buffer += "\tSCBD 2 1 2 {decenterx} {decentery} 0.000000000000E+000 0.000000000000E+000 0.000000000000E+000\n".format(
                decenterx=-surface.decenterx, decentery=-surface.decentery)
        self.buffer += "\tCOMM {}\n".format(surface.comment)
        if isinstance(surface.thickness, str):
            if surface.thickness[0] == "P":
                self.buffer += "\tDISZ {}\n".format(0)
                self.buffer += "\tPZUP {}\n".format(surface.thickness[1:])
            if surface.thickness[0] == "M":
                self.buffer += "\tDISZ {}\n".format(0)
                self.buffer += "\tMAZH {} {}\n".format(0, 1)
            else:
                self.buffer += "\tDISZ {}\n".format(surface.thickness)
        else:
            self.buffer += "\tDISZ {}\n".format(surface.thickness)

        self.buffer += "\tCONI {}\n".format(surface.conic)
        self.buffer += "\tDIAM 0.000000000000E+000 0 0\n"
        if surface.glass != None:
            self.buffer += "\tGLAS {} 0 0 1.50000000 40.00000000 0.00000000 0 0 0 0.00000000 0.00000000\n".format(
                surface.glass)
        self.buffer += "\tPOPS 0 0 0 0 0 0 0 0 1 1 1.000000000000E+000 1.000000000000E+000\n"
        if surface.stop:
            self.buffer += "\tSTOP\n"
        if surface.aperturetype == "circular":
            self.buffer += "\tCLAP 0.000000000000E+000 {}\n".format(surface.radius)
        if surface.aperturetype == "rectangular":
            self.buffer += "\tSQAP {} {}\n".format(surface.xsize, surface.ysize)
        if hasattr(surface, "adecenterx"):
            self.buffer += "\tOBDC {} {}\n".format(surface.adecenterx, surface.adecentery)
            
    def grating_surface(self,surface,density,order):
        self.buffer += "SURF {}\n".format(self.surfn)
        self.surfn  += 1
        self.buffer += "\tTYPE DGRATING\n"
        self.buffer += "\tCURV {} 0 0.000000000000E+000 0.000000000000E+000\n".format(surface.curvature)
        self.buffer += "\tSLAB {}\n".format(self.surfn)
        self.buffer += "\tSCBD 1 0 0 {decenterx} {decentery} 0.000000000000E+000 0.000000000000E+000 0.000000000000E+000\n".format(decenterx=surface.decenterx,decentery=surface.decentery)
        if surface.reverse:
            self.buffer += "\tSCBD 2 1 2 {decenterx} {decentery} 0.000000000000E+000 0.000000000000E+000 0.000000000000E+000\n".format(decenterx=-surface.decenterx,decentery=-surface.decentery)
        self.buffer += "\tCOMM {}\n".format(surface.comment)
        if isinstance(surface.thickness,str):
            if surface.thickness[0]=="P":
                self.buffer += "\tDISZ {}\n".format(0)
                self.buffer += "\tPZUP {}\n".format(surface.thickness[1:])
            if surface.thickness[0]=="M":
                self.buffer += "\tDISZ {}\n".format(0)
                self.buffer += "\tMAZH {} {}\n".format(0,0)
            else:
                self.buffer += "\tDISZ {}\n".format(surface.thickness)
        else:
            self.buffer += "\tDISZ {}\n".format(surface.thickness)
        
        self.buffer += "\tCONI {}\n".format(surface.conic)
        self.buffer += "\tDIAM 0.000000000000E+000 0 0\n"
        if surface.glass!=None:
            self.buffer += "\tGLAS {} 0 0 1.50000000 40.00000000 0.00000000 0 0 0 0.00000000 0.00000000\n".format(surface.glass)
        self.buffer += "\tPOPS 0 0 0 0 0 0 0 0 1 1 1.000000000000E+000 1.000000000000E+000\n"
        if surface.stop:
            self.buffer += "\tSTOP\n"
        if surface.aperturetype =="circular":
            self.buffer+="\tCLAP 0.000000000000E+000 {}\n".format(surface.radius)
        if surface.aperturetype =="rectangular":
            self.buffer+="\tSQAP {} {}\n".format(surface.xsize,surface.ysize)
        if hasattr(surface,"adecenterx"):
            self.buffer+="\tOBDC {} {}\n".format(surface.adecenterx,surface.adecentery)
        self.buffer += "\tPARM 1  {}\n".format(density)
        self.buffer += "\tPARM 2  {}\n".format(order)
            
            
    def paraxial_surface(self,thickness,focal_length,comment=""):
        self.buffer += "SURF {}\n".format(self.surfn)
        self.surfn  += 1
        self.buffer += "\tTYPE PARAXIAL\n"
        self.buffer += "\tCURV 0 0 0.000000000000E+000 0.000000000000E+000\n"
        self.buffer += "\tSLAB {}\n".format(self.surfn)
        self.buffer += "\tDIAM 0.000000000000E+000 0 0\n"
        if isinstance(thickness,str):
            if thickness[0]=="P":
                self.buffer += "\tDISZ {}\n".format(0)
                self.buffer += "\tPZUP {}\n".format(thickness[1:])
            if thickness[0]=="M":
                self.buffer += "\tDISZ {}\n".format(0)
                self.buffer += "\tMAZH {} {}\n".format(0,float(thickness[1:]))
            else:
                self.buffer += "\tDISZ {}\n".format(thickness)
        else:
            self.buffer += "\tDISZ {}\n".format(thickness)
        self.buffer += "\tPARM 1  {}\n".format(focal_length)
        self.buffer += "\tPARM 2  {}\n".format(0)
        self.buffer += "\tPOPS 0 0 0 0 0 0 0 0 1 1 1.000000000000E+000 1.000000000000E+000\n"
        self.buffer += "\tCOMM {}\n".format(comment)
        
     
    def coordbreak_surface(self,thickness,coordbreak,comment=""):
        self.buffer += "SURF {}\n".format(self.surfn)
        self.surfn  += 1
        self.buffer += "\tTYPE COORDBRK\n"
        self.buffer += "\tCURV 0 0 0.000000000000E+000 0.000000000000E+000\n"
        self.buffer += "\tSLAB {}\n".format(self.surfn)
        self.buffer += "\tDIAM 0.000000000000E+000 0 0\n"
        self.buffer += "\tDISZ {}\n".format(thickness)
        self.buffer += "\tCOMM {}\n".format(comment)
        for k in coordbreak.__dict__.keys():
            param=getattr(coordbreak,k)
            if isinstance(param,str):
                if param[0]=="P":
                    self.buffer += "\tPARM {}  {}\n".format(cbrk[k],0)
                    self.buffer += "\tPPAR {}  {}\n".format(cbrk[k],param[1:])
            else:
                self.buffer += "\tPARM {}  {}\n".format(cbrk[k],param)
        self.buffer += "\tPOPS 0 0 0 0 0 0 0 0 1 1 1.000000000000E+000 1.000000000000E+000\n"
    
        
    def name(self,name):
        self.buffer+= "NAME {}\n".format(name)
        
    def save(self,file_name):
        f=open(file_name,"w+")
        f.write(self.buffer)
        f.close()
        
        
        
        
        
if __name__=="__main__":
    
    # case one
    op = optical_model()
    op.fields(fields=n.array([[0,0],[0,1],[0,-1]]))
    op.wavelength_flat(wavelength=n.arange(5))
    op.nconfigs(4)
    op.wave_config(wavelengths=n.arange(4*5).reshape(5,4))
    op.ENP_diameter(100)
    s1=surface(0,0,"INFINITY")
    s2=surface(1/350.,1,100,glass="MIRROR",stop=True,decentery=50)
    s3=surface(0,0,0)
    op.standard_surface(s1)
    op.standard_surface(s2)
    op.standard_surface(s3)
    op.save("test1.zmx")
    
    # case 2
    op = optical_model()
    op.fields(fields=n.array([[0,0],[0,1],[0,-1]]))
    op.wavelength_flat(wavelength=n.arange(5))
    op.nconfigs(4)
    op.wave_config(wavelengths=n.arange(4*5).reshape(5,4))
    op.NA_aperture(0.15,telecentric=False)
    s1=surface(0,0,100)
    s2=surface(1/350.,1,100,glass="MIRROR",stop=True)
    s3=surface(0,0,0)
    op.standard_surface(s1)
    op.standard_surface(s2)
    op.standard_surface(s3)
    op.save("test2.zmx")
    
    
    # case three
    op = optical_model()
    op.fields(fields=n.array([[0,0],[0,1],[0,-1]]))
    op.wavelength_flat(wavelength=n.arange(5))
    op.nconfigs(4)
    op.wave_config(wavelengths=n.arange(4*5).reshape(5,4))
    op.ENP_diameter(100)
    s1=surface(0,0,"INFINITY")
    s2=surface(1/350.,1,100,glass="MIRROR",stop=False,aperturetype="circular",radius=45,adecentery=30,adecenterx=0)
    s3=coordbreak(decenterx=4,tilty=45)
    s4=surface(1/450.,0.4,100,stop=True,aperturetype="rectangular",xsize=45,ysize=32,adecentery=30,adecenterx=0)
    s5=surface(1/350.,1,"P2 1 0")
    s7=coordbreak(decenterx="P-1 1 0",tilty="P2 1 0")
    s8=surface(0,0,100)
    s9=surface(0,0,"M")
    s10=surface(0,0,0)
    
    op.standard_surface(s1)
    op.standard_surface(s2)
    op.coordbreak_surface(10,s3)
    op.standard_surface(s4)
    op.standard_surface(s5)
    op.paraxial_surface(100,100)
    op.coordbreak_surface(10,s7)
    op.grating_surface(s8,.3125,56)
    op.standard_surface(s9)
    op.standard_surface(s10)
    op.save("test3.zmx")
    
    