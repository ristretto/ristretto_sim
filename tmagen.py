#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 10 17:35:26 2019

@author: chazelas
"""

import numpy as n
import zmxgen
import pylab as plt


def korsh_livre_3(s1,d1,d2,om1,om2,t1):
    v1 = 1/s1
    vp1 = (1-om1)/d1
    v2 = vp1/om1
    vp2 = (1-om2)/d2
    v3 = vp2/ om2
    
    c1 = 0.5 *(v1+vp1)
    c2 = 0.5 *(v2+vp2)
    c3 = c2 - c1
    vp3 = 2* c3 - v3
    f = - 1./(2*(c1-c2+c3+2*c1*(c2-c3)*d1+2*c3*(c1-c2)*d2+4*c1*c2*c3*d1*d2))
    
    
    Dd3 = (2*om1*(v1+vp3)-v1**2*d1-om1*om2*vp3**2*(om1*om2*d1-2*d2))/(4*om1*om2**2*d2*(om1*om2*d1-d2)*c3**3)
    Dd2 = (v1**2-om1**2*om2**2*vp3**2+4*om1**2*om2**3*(om1*om2*d1-d2)*c3**3*Dd3)/(4*om1**3*d1*c2**3)
    Dd1 = om1**4/c1**3*(c2**3*Dd2-om2**4*c3**3*Dd3)
    
    
    m2 = -d2*(1-om1)/(om1*d1*(1-om2))
    
    de1 = Dd1 - ((1-om1-v1*d1)/(1-om1+v1*d1))**2
    #de2 = Dd2 - ((om1*d1*(1-om1)-d2*(1-om2))/(om1*d1*(1-om1)+d2*(1-om2)))**2
    de2 = Dd2 - ((1+m2)/(1-m2))**2
    de3 = Dd3 - ((om2*vp3*d2-1+om2)/(om2*vp3*d2+1-om2))**2
    
    return {"c1":c1,"c2":c2,"c3":c3,"r1":1/c1,"r2":1/c2,"r3":1/c3,"vp3":vp3,"sp3":1/vp3,
           "de1":de1,"de2":de2,"de3":de3,"f":f}



def plot_mirror(h,dh,z0,c,k):
    r = n.linspace(h-dh,h+dh,100)
    z = z0+c*r**2/(1+n.sqrt(1-(1+k)*c**2*r**2))
    plt.plot(z,r,"k")
    
    
def korsh_livre_3f(s1,d1,d2,h1,h2,f,sm = True):
    
    
    
    
    
    om1 = h2/h1
    if sm:
        om2 = (2*d1*om1 + d2*om1**2 - 2*d2*om1 + d2)/(2*d1*om1) - n.sqrt(-d2*f*(4*d1**2*om1 - 4*d1*f*om1**3 + 8*d1*f*om1**2 - 4*d1*f*om1 - d2*f*om1**4 + 4*d2*f*om1**3 - 6*d2*f*om1**2 + 4*d2*f*om1 - d2*f))/(2*d1*f*om1)
    else:
        om2 =  (2*d1*om1 + d2*om1**2 - 2*d2*om1 + d2)/(2*d1*om1) + n.sqrt(-d2*f*(4*d1**2*om1 - 4*d1*f*om1**3 + 8*d1*f*om1**2 - 4*d1*f*om1 - d2*f*om1**4 + 4*d2*f*om1**3 - 6*d2*f*om1**2 + 4*d2*f*om1 - d2*f))/(2*d1*f*om1)
    print(om2)
    v1 = 1/s1
    vp1 = (1-om1)/d1
    v2 = vp1/om1
    vp2 = (1-om2)/d2
    v3 = vp2/ om2
    
    c1 = 0.5 *(v1+vp1)
    c2 = 0.5 *(v2+vp2)
    c3 = c2 - c1
    vp3 = 2* c3 - v3
    f = - 1./(2*(c1-c2+c3+2*c1*(c2-c3)*d1+2*c3*(c1-c2)*d2+4*c1*c2*c3*d1*d2))
    
    
    Dd3 = (2*om1*(v1+vp3)-v1**2*d1-om1*om2*vp3**2*(om1*om2*d1-2*d2))/(4*om1*om2**2*d2*(om1*om2*d1-d2)*c3**3)
    Dd2 = (v1**2-om1**2*om2**2*vp3**2+4*om1**2*om2**3*(om1*om2*d1-d2)*c3**3*Dd3)/(4*om1**3*d1*c2**3)
    Dd1 = om1**4/c1**3*(c2**3*Dd2-om2**4*c3**3*Dd3)
    
    
    m2 = -d2*(1-om1)/(om1*d1*(1-om2))
    
    de1 = Dd1 - ((1-om1-v1*d1)/(1-om1+v1*d1))**2
    #de2 = Dd2 - ((om1*d1*(1-om1)-d2*(1-om2))/(om1*d1*(1-om1)+d2*(1-om2)))**2
    de2 = Dd2 - ((1+m2)/(1-m2))**2
    de3 = Dd3 - ((om2*vp3*d2-1+om2)/(om2*vp3*d2+1-om2))**2
    
    h3 = om2*h2
    plt.figure()
    plt.plot([ -d1/5,0,-d1,-d1-d2,-d1-d2-1/vp3],[h1,h1,h2,h3,0],".-")
    plot_mirror(h1,(h1-h2)/3,0,-c1,de1)
    plot_mirror(h2,(h1-h2)/3,-d1,-c2,de2)    
    plot_mirror(h3,(h1-h2)/3,-d1-d2,-c3,de3)
    
    
    return {"c1":c1,"c2":c2,"c3":c3,"r1":1/c1,"r2":1/c2,"r3":1/c3,"vp3":vp3,"sp3":1/vp3,
           "de1":de1,"de2":de2,"de3":de3,"f":f}
    



def gen_tma(s1,d1,d2,h1,h2,f,fields,wavelengths,filename,sm = True):
    
    
    recipy= korsh_livre_3f(s1,d1,d2,h1,h2,f,sm = sm)
    print("coucou",recipy)
    op = zmxgen.optical_model()
    op.fields(fields=fields)
    op.wavelength_flat(wavelength=wavelengths)
    op.nconfigs(1)
    
    op.ENP_diameter(d1/7)
    s1=zmxgen.surface(0,0,"INFINITY")
    s2=zmxgen.surface(0,0,f/5)
    s3=zmxgen.coordbreak(decentery=-h1)
    if recipy["c1"] <0:
        s4=zmxgen.surface(recipy["c1"],recipy["de1"],d1,glass="MIRROR",stop=True)
        s5=zmxgen.surface(recipy["c2"],recipy["de2"],d2,glass="MIRROR")
        s6=zmxgen.surface(recipy["c3"],recipy["de3"],"M",glass="MIRROR")
    else:
        s4=zmxgen.surface(-recipy["c1"],recipy["de1"],-d1,glass="MIRROR",stop=True)
        s5=zmxgen.surface(-recipy["c2"],recipy["de2"],-d2,glass="MIRROR")
        s6=zmxgen.surface(-recipy["c3"],recipy["de3"],"M",glass="MIRROR")
    s7=zmxgen.surface(0,0,0)
    
    op.standard_surface(s1)
    op.standard_surface(s2)
    op.coordbreak_surface(0,s3)
    op.standard_surface(s4)
    op.standard_surface(s5)
    op.standard_surface(s6)
    op.standard_surface(s7)
    op.save(filename)

    
if __name__=="__main__":
     #print(korsh_livre_3(n.inf,1,-1,0.5,1.1,0))
     #print(korsh_livre_3f(n.inf,365,-365,260,105,907.1,sm=False))
     #gen_tma(n.inf,365,-365,260,105,907.1,n.array([[0,0],[0,-1],[0,1]]),n.array([0.6,0.7,0.8]),"test_korsh.zmx",sm = False)
     
     #gen_tma(n.inf,300,-400,300,100,1062.21,n.array([[0,0],[0,-1],[0,1]]),n.array([0.6,0.7,0.8]),"test_korsh_1.zmx",sm = False)
     #gen_tma(n.inf,400,-350,300,110,1062.21,n.array([[0,0],[0,-1],[0,1]]),n.array([0.6,0.7,0.8]),"korsh_Rstr.zmx",sm = False)
     gen_tma(n.inf,400,-350,300,110,1116.726,n.array([[0,0],[0,-1],[0,1]]),n.array([0.6,0.7,0.8]),"korsh116_Rstr.zmx",sm = False)
     #gen_tma(n.inf,-1100,2000,400,80,17809.439,n.array([[0,0],[0,-1],[0,1]]),n.array([0.6,0.7,0.8]),"test_korsh.zmx",sm = True)