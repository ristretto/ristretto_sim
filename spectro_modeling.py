from pylab import *
from ipywidgets import Layout, Button, Box, FloatText, Textarea, Dropdown, Label, IntSlider, interact, Output, Checkbox, RadioButtons, Text, FloatSlider
import pandas as pd
from scipy.interpolate import interp1d
import xlsxwriter
import inspect
import time
import zmxgen


gratings = pd.read_csv("liste_gratings.csv")
blaze = list(set(gratings["blaze"]))
blaze.sort()
densite = set(gratings["densite"])


#atmos = loadtxt("atmos_transmission_tapas/tapas_000001.ipac",skiprows = 21)
bare_al = loadtxt("bare_aluminum.csv")
r_al = interp1d(bare_al[:,0],bare_al[:,1]/100,bounds_error=False,fill_value="extrapolate")

n_sf11 = pd.read_excel("SF11.xlsx")
dndlSF11 = interp1d(n_sf11["wavelength"][0:-1],diff(n_sf11["n"])/(diff(n_sf11["wavelength"]*1e-3)),bounds_error=False,fill_value="extrapolate")
nSF11 = interp1d(n_sf11["wavelength"],n_sf11["n"],bounds_error=False,fill_value="extrapolate")





class Spectro_simulate():

    def __init__(self):
        self.form_item_layout = Layout(
            display='flex',
            flex_flow='row',
            justify_content='space-between'
        )
        short_input= Layout(width="80px",display="flex")
        short_output = Layout(width="80px")
        style = {'description_width': 'initial'}
        self.Nfiber = FloatText(7,layout=short_input)
        self.FiberAngle = FloatText(10.5, layout=short_input)
        self.lmin = FloatText(620,layout=short_input)
        self.lmax = FloatText(840,layout=short_input)
        self.elmin = FloatText(layout=short_input)
        self.elmax = FloatText(layout=short_input)
        self.Bldeg = Dropdown(value=70.0,options=blaze,layout=short_input)
        self.real_d = Dropdown(options=densite,layout=short_input)
        self.use_real_grating = Checkbox(value=False, description='Use real grating')
        self.reduce_sampling_or_out_of_ccd = RadioButtons(value ="Keep resolution" , options=["Reduce resolution","Keep resolution","Tune"])
        self.resolution_slider = FloatSlider(value = 1, min = 0, max = 1)
        self.opt_d = FloatText(layout=short_input)
        self.order_min = FloatText(layout=short_input)
        self.order_max = FloatText(layout=short_input)
        self.compute = Button(description='Compute')
        self.exportBtn =Button(description='Export result')
        self.exportZMXBtn =Button(description='Export ZMX')
        zmx_exp_option = (("harps_like",1),("paraxial lenses simple pass",2),("paraxial lenses double pass",3))
        self.exportZMXCho = Dropdown(value=2 ,options=zmx_exp_option,layout=Layout(width='300px'))
        self.spl_PSF = FloatText(3,layout=short_input)
        self.pix_size = FloatText(15,layout=short_input)
        self.nx = FloatText(4096,layout=short_input)
        self.f_c = FloatText(layout=short_input)
        self.Res = FloatText(150000,layout=short_input)
        self.No = FloatText(layout=short_input)
        self.InterFiber = FloatText (9,layout=short_input)
        self.InterOrder = FloatText (4,layout=short_input)
        self.orderCrossThickness = FloatText (2,layout=short_input)
        self.g_length = FloatText(description='L')
        self.g_height = FloatText(description='H')
        self.max_length = Dropdown(description='max L')
        self.max_height= Dropdown(description='max H')
        self.Reference = Dropdown(description="Reference")
        self.Manufacturer = Dropdown(description="Manufacturer")
        self.OrderOverlap = FloatText(10,layout=short_input)
        self.ActualSampling = FloatText(layout=short_input)
        self.MarginLargest_order = FloatText(5,layout=short_input)
        self.prism_thickness = FloatText(layout=short_input)
        self.prism_margin = FloatText(5,layout=short_input)
        self.prism_apex = FloatText(layout=short_input)
        self.prism_input_angle = FloatText(layout=short_input)


        self.cd_Opt_d = FloatText(layout=short_input)
        self.bet = FloatText(layout=short_input)
        self.m_p = FloatText(layout=short_input)
        self.diffraction_margin = FloatText(1.75,layout=short_input)

        self.Order = Dropdown()
        lll = Layout(width='200px')
        self.limin = FloatText(description="$\lambda_{min}$",layout=lll)
        self.licen = FloatText(description="$\lambda_{cen}$",layout=lll)
        self.limax = FloatText(description="$\lambda_{max}$",layout=lll)
        self.cameraFN = FloatText(layout=short_input)
        self.Field_Camera = FloatText(layout=short_input)

        self.NA_fiber = FloatText(0.12,layout=short_input)
        self.OuterDiameterFiber = FloatText(125,layout=short_input)
        self.Fiber_distance = FloatText(layout=short_input)
        self.Mean_order_tilt = FloatText(layout=short_input)

        self.real_d.options = sort(gratings["densite"][gratings["blaze"] == self.Bldeg.value])
        self.Title = Text(description="Title of the Spectro")
        self.CollFN = FloatText(10,description = "Collimateur Focal Length")

        self.label_res =Label(value='')
        self.out = Output()
        self.form_items = [
            Box([Label(value='Fiber Number'), self.Nfiber,
                 Label(value='Angle'), self.FiberAngle,
                 Label(value='$\lambda$ min'), self.lmin,
                 Label(value='$\lambda$ max'), self.lmax,
                 Label(value='Blaze angle'),self.Bldeg], layout=self.form_item_layout),

            
            Box([Label(value='PSF sampling in pixel'), self.spl_PSF,
                 Label(value='Resolution'), self.Res,
                Label(value='Number of Pixels'), self.nx,
                
                ], layout=self.form_item_layout),
            
            
            Box([Label(value='Distance between spectrums in pixels'), self.InterFiber,
                 Label(value='Additional Distance between orders in pixels'), self.InterOrder], layout=self.form_item_layout),
            
            Box([Label(value='Sampling in the cross direction direction'), self.orderCrossThickness],layout=self.form_item_layout),
            
            Box([Label(value="Margin on the sides of the CCD in %"), self.MarginLargest_order,
                 Label(value='Pixel size in $\mu m$'),self.pix_size,
                 Label(value='Order Overlap in %'),self.OrderOverlap], layout=self.form_item_layout),
            Box([Label(value='Real Density'),self.use_real_grating,self.real_d], layout=self.form_item_layout),
            Box([Label(value='How to deal with imperfection'),self.reduce_sampling_or_out_of_ccd], layout=self.form_item_layout),
            Box([Label(value='Resolution Tuning'), self.resolution_slider],
                layout=self.form_item_layout),
            Box([Label(value='Optimal density'),self.opt_d,
                 Label(value='Real Sampling'),self.ActualSampling], layout=self.form_item_layout),
            Box([Label(value='Margin on the diffraction'),self.diffraction_margin],layout=self.form_item_layout),
            Box([Label(value='Size of grating'),self.g_length,self.g_height,], layout=self.form_item_layout),
            Box([Label(value='mother grating'),self.max_length,self.max_height], layout=self.form_item_layout),
            Box([Label(value='mother grating'),self.Reference,self.Manufacturer], layout=self.form_item_layout),
            
            Box([Label(value='Order Min'),self.order_min,
                Label(value='Order Max'),self.order_max,
                Label(value='Number of Orders'),self.No], layout=self.form_item_layout),
                        
            Box([Label(value='Focal lenght of the camera'),self.f_c,
                Label(value='FNumber Camera'),self.cameraFN,
                Label(value='Field of Camera'),self.Field_Camera], layout=self.form_item_layout),
            
            Box([Label(value='Prism Thickness'),self.prism_thickness,   
                Label(value='Prism Apex angle'),self.prism_apex,  
                Label(value='Prism margin'),self.prism_margin,
                Label(value='Prism input angle'),self.prism_input_angle], layout=self.form_item_layout),  
            
            Box([Label(value='Cross disperser grating period '),self.cd_Opt_d,
                Label(value='Cross disperser output angle '),self.bet,
                Label(value='Cross disperser density '),self.m_p], layout=self.form_item_layout),
            
            Box([Label(value='NA fiber '),self.NA_fiber,
                Label(value='Fiber_distance '),self.Fiber_distance,
                Label(value="Fiber outer diameter"), self.OuterDiameterFiber,
                Label(value='Order Tilt'),self.Mean_order_tilt], layout=self.form_item_layout),
            
                    
            Box([Label(value='effective $\lambda$ min'), self.elmin,
                Label(value='effective $\lambda$ max'), self.elmax,
                Label(value='Order'), self.Order], layout=self.form_item_layout),
            Box([self.limin,self.licen,self.limax], layout=self.form_item_layout),



            Box([self.label_res,self.Title,self.CollFN], layout=self.form_item_layout),
            Box([self.compute,self.exportBtn,self.exportZMXCho,self.exportZMXBtn], layout=self.form_item_layout),
            Box([self.out], layout=self.form_item_layout)
        ]

        self.form = Box(self.form_items, layout=Layout(
            display='flex',
            flex_flow='column',
            border='solid 1px',
            justify='space-around',
            width='70%'
        ))
        
        
        display(self.form)
        self.Bldeg.observe(self.change_available_d,names="value")
        self.Order.observe(self.update_wave,names="value")
        self.compute.on_click(self.compute_spectro)
        self.exportBtn.on_click(self.export_spectro)
        self.exportZMXBtn.on_click(self.export_spectro_zmx)
        self.resolution_slider.observe(self.tune_resolution,names="value")
        self.reduce_sampling_or_out_of_ccd.observe(self.tune_resolution, names="value")
        self.use_real_grating.observe(self.tune_resolution, names="value")

    
    def compute_spectro(self,b,export_graph=False):
        self.out.clear_output()
        with self.out:
            self.c_m=(1+self.OrderOverlap.value/100.)/2.
            ##print("coucou")
            c_pix = 1- self.MarginLargest_order.value/100

            Dlfsr_max = ( self.nx.value*self.lmax.value)/(self.spl_PSF.value*self.Res.value)
            if not(self.use_real_grating.value):
                self.opt_d.value = Dlfsr_max*2*sin(self.Bldeg.value*pi/180)/self.lmax.value**2*1e6
            else:
                self.opt_d.value = self.real_d.value
                self.Manufacturer.options = gratings["fabricant"][(gratings["blaze"]==self.Bldeg.value)&(gratings["densite"]==self.real_d.value)]
                self.Reference.options = gratings["reference"][(gratings["blaze"]==self.Bldeg.value)&(gratings["densite"]==self.real_d.value)]
                self.max_length.options = gratings["largeur"][(gratings["blaze"]==self.Bldeg.value)&(gratings["densite"]==self.real_d.value)]
                self.max_height.options = gratings["longueur "][(gratings["blaze"]==self.Bldeg.value)&(gratings["densite"]==self.real_d.value)]


            self.label_res.value = "Computation Done"
            self.order_min.value = round(2*sin(self.Bldeg.value*pi/180)/(self.opt_d.value*1000*self.lmax.value*0.000000001))
            self.order_max.value = round(2*sin(self.Bldeg.value*pi/180)/(self.opt_d.value*1000*self.lmin.value*0.000000001))
            self.No.value = self.order_max.value - self.order_min.value
            Dlb = 2*sin(self.Bldeg.value*pi/180)/self.opt_d.value/(self.order_max.value*(self.order_max.value-1))
            #print(Dlb)
            Dbfsr_max = (self.c_m*2)*2 * sin(self.Bldeg.value*pi/180)/(self.order_min.value*cos(self.Bldeg.value*pi/180))

            loc = 2*sin(self.Bldeg.value*pi/180)/(self.order_min.value*self.opt_d.value)*1000000
            dl = loc / self.order_min.value
            Dbres = loc/self.Res.value / dl * Dbfsr_max

            if "Reduce" in self.reduce_sampling_or_out_of_ccd.value:
                self.f_c.value = c_pix * self.nx.value * self.pix_size.value * 1e-3 / Dbfsr_max
            elif "Keep" in self.reduce_sampling_or_out_of_ccd.value:
                self.f_c.value = self.spl_PSF.value * self.pix_size.value *1e-3 / Dbres
            else:
                f_c1 = self.f_c.value = c_pix * self.nx.value * self.pix_size.value * 1e-3 / Dbfsr_max
                f_c2 = self.spl_PSF.value * self.pix_size.value *1e-3 / Dbres
                self.f_c.value = (self.resolution_slider.value * f_c1 + (1-self.resolution_slider.value) * f_c2)

            self.ActualSampling.value = Dbres * self.f_c.value / (self.pix_size.value*1e-3)
            ##print(Dbfsr_max)

            self.Field_Camera.value = (self.nx.value * self.pix_size.value * 1e-3 / self.f_c.value ) *180/pi


            self.g_length.value = self.Res.value/ self.order_min.value / self.opt_d.value * self.diffraction_margin.value
            print("length grating : ",self.g_length.value,self.Res.value / self.order_max.value / self.opt_d.value * self.diffraction_margin.value)
            self.g_height.value = self.g_length.value / tan(self.Bldeg.value*pi/180)

            dth_dl = self.pix_size.value * 1e-3 * ((self.orderCrossThickness.value+self.InterFiber.value) * self.Nfiber.value + self.InterOrder.value) / (self.f_c.value * Dlb)

            self.cameraFN.value = self.f_c.value / self.g_height.value

            #print(dth_dl)
            
            
            # Distance between the fibers in the focal plane
            
            f_col_min = 1/(2*self.NA_fiber.value) * self.g_height.value #/ self.diffraction_margin.value
            self.Fiber_distance.value = (self.InterFiber.value * self.pix_size.value * 1e-3 )/self.f_c.value*f_col_min * 1e3
            self.FiberAngle.value = arctan(self.Fiber_distance.value/self.OuterDiameterFiber.value)*180/pi
            offset_fiber_tilt = self.Nfiber.value*self.OuterDiameterFiber.value * cos(self.FiberAngle.value * pi / 180) / self.Fiber_distance.value * self.InterFiber.value
            
            
             # Calculation of the different orders wavelength              
            
            lomin = 2*sin(self.Bldeg.value*pi/180)/(self.order_max.value*self.opt_d.value)*1000000
            lomax = 2*sin(self.Bldeg.value*pi/180)/(self.order_min.value*self.opt_d.value)*1000000
            self.elmin.value =  lomin - self.c_m * lomin / self.order_max.value
            self.elmax.value =  lomax + self.c_m * lomax / self.order_min.value
            
            self.Order.options = arange(self.order_min.value,self.order_max.value+1)
            self.Order.value = self.order_max.value
            self.licen.value = 2*sin(self.Bldeg.value*pi/180)/(self.Order.value*self.opt_d.value)*1000000
            self.limin.value = self.licen.value - self.c_m * self.licen.value / self.Order.value
            self.limax.value = self.licen.value + self.c_m * self.licen.value / self.Order.value

            # Efficiency of the orders

            #plot(atmos[:,0],atmos[:,1],"k",linewidth=0.2)
            #print("density : ", self.opt_d.value)
            llll = linspace(self.elmin.value,self.elmax.value,100000)
            eff_g = zeros(llll.shape)
            eff_gd = zeros(llll.shape)
            oooo = arange(0,int(self.order_max.value+1)+20)
            loc0 = 2 * sin(self.Bldeg.value * pi / 180) / (oooo * self.opt_d.value) * 1000000
            beth_c0 = arcsin(oooo * loc0 * 1e-6 * self.opt_d.value - sin(self.Bldeg.value * pi / 180))
            (loc00,or00)=meshgrid(loc0,oooo)
            beth00 = arcsin(or00 * loc00 * 1e-6 * self.opt_d.value - sin(self.Bldeg.value * pi / 180))
            #beth00[tril_indices(beth00.shape[0])]=beth00.T[tril_indices(beth00.shape[0])]

            
            figure()
            #imshow(beth00*180/pi)

            sig = 1e6 / self.opt_d.value
            delta00 = ( sig * cos(self.Bldeg.value * pi / 180) / (loc00) * sin(beth00 - beth_c0[or00-or00.min()]))
            eff00 = sinc(delta00)**2
            imshow(eff00,extent=[0,self.order_max.value+20,0,self.order_max.value+20])
            colorbar()
            eps0 = 1/nansum(eff00[25:,25:],axis=0)

            figure()
            plot(oooo[25:],eps0)

            ##print(eps0)

            figure(figsize=(14, 4))


            # calculation of the offset to center the orders

            i = self.order_min.value

            loc = 2 * sin(self.Bldeg.value * pi / 180) / (i * self.opt_d.value) * 1000000
            dl = loc / i * self.c_m
            ll = linspace(loc - dl, loc + dl, 10000)

            # #print(i,loc-dl,loc+dl)
            beth = arcsin(i * ll * 1e-6 * self.opt_d.value - sin(self.Bldeg.value * pi / 180))
            beth_c = arcsin(i * loc * 1e-6 * self.opt_d.value - sin(self.Bldeg.value * pi / 180))
            pix = (beth - beth_c) * self.f_c.value * 1000 / self.pix_size.value + self.nx.value / 2

            pmin = pix.min()

            pmax = pix.max()

            off = (self.nx.value / 2 - (pmin + pmax) / 2)




            for i in range(int(self.order_min.value),int(self.order_max.value)+1):
                loc = 2*sin(self.Bldeg.value*pi/180)/(i*self.opt_d.value)*1000000
                dl = loc / i * self.c_m
                ll = linspace(loc-dl,loc+dl,100)
                ml = (llll >= loc-dl)&(llll <= loc+dl)

                ##print(i,loc-dl,loc+dl)
                beth = arcsin(i*ll*1e-6*self.opt_d.value-sin(self.Bldeg.value*pi/180))
                bethg = arcsin(i*llll[ml]*1e-6*self.opt_d.value-sin(self.Bldeg.value*pi/180))
                beth_c = arcsin(i*loc*1e-6*self.opt_d.value-sin(self.Bldeg.value*pi/180))
                pix = (beth-beth_c)*self.f_c.value*1000/self.pix_size.value + self.nx.value/2
                pix_g = (bethg-beth_c)*self.f_c.value*1000/self.pix_size.value + self.nx.value/2
                ##print(pix)
                beth[(pix < 0)|(pix > self.nx.value)] = nan

                sig = 1e6/self.opt_d.value
                ##print(loc, sig,i * loc * 1e-6 * self.opt_d.value,beth_c,ptp(beth)*180/pi)

                eff = sinc(pi*(ll-loc)/(8*dl))**2
                delta = (pi*sig*cos(self.Bldeg.value*pi/180)/(ll)*sin(beth-beth_c))
                deltag = (pi * sig * cos(self.Bldeg.value * pi / 180) / (llll[ml]) * sin(bethg - beth_c))

                eff1 = eps0[i-25]*(sin(delta)/delta)**2

                eff_g0 = eps0[i-25]*(sin(deltag)/deltag)**2
                eff_g0d = eff_g0.copy()
                eff_g0[(pix_g < 0-offset_fiber_tilt/2-off)|(pix_g > self.nx.value -offset_fiber_tilt/2-off)] = 0
                eff_g0d[(pix_g < 0 + offset_fiber_tilt/2-off) | (pix_g > self.nx.value + offset_fiber_tilt/2-off)] = 0
                eff_g[ml] += eff_g0
                eff_gd[ml] += eff_g0d
                plot(ll, eff1, "r")


            xlim(self.elmin.value,self.elmax.value)
            xlabel("$\lambda$ in nm")
            ylabel("Efficiency")
            title("Spectrograph Efficiency")
            if export_graph:
                savefig("efficiency.png",dpi=150)
            #show()

            figure(figsize=(14, 4))
            plot(llll,eff_g)
            fill_between(llll,eff_g,0,alpha = 0.4)
            fill_between(llll, eff_gd, 0, alpha=0.4)

            xlim(self.elmin.value, self.elmax.value)
            xlabel("$\lambda$ in nm")
            ylabel("Efficiency")
            title("Spectrograph Efficiency")
            if export_graph:
                savefig("efficiency_2.png",dpi=150)
            # Cross dispersion

            dl_order_b = 2*sin(self.Bldeg.value*pi/180)*(1/((self.order_max.value-1)*self.opt_d.value)-1/(self.order_max.value*self.opt_d.value))
            dl_order_r = 2*sin(self.Bldeg.value*pi/180)*(1/((self.order_min.value)*self.opt_d.value)-1/((self.order_min.value+1)*self.opt_d.value))
            #print(dl_order_b)
            height_per_order = self.Nfiber.value * (self.InterFiber.value+self.orderCrossThickness.value) + self.InterOrder.value
            dth_dl = self.pix_size.value*1e-3 * height_per_order /(self.f_c.value*dl_order_b)
            dth_dl_r = self.pix_size.value*1e-3 * height_per_order /(self.f_c.value*dl_order_r) * 0.9
            #print(height_per_order)
            #print(self.pix_size.value*1e-3 * height_per_order)
            #print(dth_dl)
            #print(dth_dl_r)
            
            
            
            # average tilt of the orders
            
            om = (self.order_max.value+self.order_min.value)/2
            dbLoc = (self.c_m*2)*2* sin(self.Bldeg.value*pi/180)/(om*cos(self.Bldeg.value*pi/180))
            dl_order = 2*sin(self.Bldeg.value*pi/180)*(1/((om)*self.opt_d.value)-1/((om+1)*self.opt_d.value))
            self.Mean_order_tilt.value = arctan(dl_order*dth_dl/dbLoc)*180/pi
            
            # case of the grating
            
            self.cd_Opt_d.value = dth_dl / sqrt(dth_dl**2*((self.lmin.value+self.lmax.value)/2*1e-6)**2+1)
            self.bet.value = arcsin(self.cd_Opt_d.value*((self.lmin.value+self.lmax.value)/2*1e-6))*180/pi
            self.m_p.value = dth_dl/sqrt(dth_dl**2*((self.lmin.value+self.lmax.value)/2*0.000001)**2+1)
            self.m_p.value = dth_dl/sqrt(dth_dl**2*(self.lmin.value*0.000001)**2+1)



            # case of the prism
            

            #print(dndlSF11(lomin/1000.))
            #print(self.g_height.value)
            #dth_dl1 = 1.25 * dth_dl
            self.prism_thickness.value = dth_dl_r * self.g_height.value / dndlSF11(lomax/1000.)
            self.prism_apex.value = 2*arcsin(dth_dl_r /(sqrt(dth_dl_r**2*nSF11(lomax/1000)**2+4*dndlSF11(lomax/1000.)**2)))*180/pi
            self.prism_input_angle.value = arcsin(nSF11((lomin+lomax)/2/1000)*sin(self.prism_apex.value*pi/180/2))*180/pi
            self.apex_dbl = 2*arcsin((dth_dl_r/2) /(sqrt((dth_dl_r/2)**2*nSF11(lomax/1000)**2+4*dndlSF11(lomax/1000.)**2)))*180/pi
            self.inp_a_dbl = arcsin(nSF11((lomin+lomax)/2/1000)*sin(self.apex_dbl*pi/180/2))*180/pi
            print("apex double pasage : ",self.apex_dbl)
            print("double passage input angle : ",self.inp_a_dbl)

            # Orders on the CCD grating

            figure(figsize=(14,6))
            subplot(121)
            col=["r","b"]
            yorder = self.InterFiber.value
            for i in range(int(self.order_min.value),int(self.order_max.value)+1):
                dl_order = 2*sin(self.Bldeg.value*pi/180)*(1/((i)*self.opt_d.value)-1/((i+1)*self.opt_d.value))
                dy = dl_order*dth_dl/2*self.f_c.value/(self.pix_size.value*1e-3)
                if i!=self.order_min.value:
                    yorder += dl_order/dl_order_b * height_per_order
                dbLoc = (self.c_m*2)*2* sin(self.Bldeg.value*pi/180)/(i*cos(self.Bldeg.value*pi/180))
                plot([-dbLoc/2*self.f_c.value/(self.pix_size.value*1e-3),
                      dbLoc/2*self.f_c.value/(self.pix_size.value*1e-3)],
                     [yorder,yorder+2*dy],col[i%2])
                plot([-dbLoc/2*self.f_c.value/(self.pix_size.value*1e-3),
                      dbLoc/2*self.f_c.value/(self.pix_size.value*1e-3)],
                     [yorder+height_per_order,yorder+height_per_order+2*dy],col[i%2])

                plot([-self.nx.value/2, self.nx.value/2, self.nx.value/2, -self.nx.value/2, -self.nx.value/2],
                     [0, 0, self.nx.value, self.nx.value, 0], "r")
            axis("equal")
            title("Grating Cross Disperser")
            
            #show()

            # Orders on the CCD with prism

            subplot(122)
            col=["r","b"]
            yorder = self.InterFiber.value


            for i in range(int(self.order_min.value),int(self.order_max.value)+1):
                lo = 2*sin(self.Bldeg.value*pi/180)/(i*self.opt_d.value)*1000000
                dl_order = 2*sin(self.Bldeg.value*pi/180)*(1/((i)*self.opt_d.value)-1/((i+1)*self.opt_d.value))
                dthdl_o = -2* sin(self.prism_apex.value/2*pi/180)/sqrt(1-nSF11(lo/1000)**2*sin(self.prism_apex.value/2*pi/180)**2) * dndlSF11(lo/1000.)
                dy = dl_order*dthdl_o/2*self.f_c.value/(self.pix_size.value*1e-3)
                if i!=self.order_min.value:
                    yorder += (dl_order*dthdl_o)/(dl_order_r*dth_dl_r) * height_per_order
                dbLoc = (self.c_m*2)*2* sin(self.Bldeg.value*pi/180)/(i*cos(self.Bldeg.value*pi/180))
                plot([-offset_fiber_tilt/2-dbLoc/2*self.f_c.value/(self.pix_size.value*1e-3),
                      -offset_fiber_tilt/2+dbLoc/2*self.f_c.value/(self.pix_size.value*1e-3)],
                     [yorder,yorder+2*dy],col[i%2])
                plot([+offset_fiber_tilt/2-dbLoc/2*self.f_c.value/(self.pix_size.value*1e-3),
                      +offset_fiber_tilt/2+dbLoc/2*self.f_c.value/(self.pix_size.value*1e-3)],
                     [yorder+height_per_order,yorder+height_per_order+2*dy],col[i%2])

                plot([-self.nx.value / 2, self.nx.value / 2, self.nx.value / 2, -self.nx.value / 2, -self.nx.value / 2],
                     [0, 0, self.nx.value, self.nx.value, 0], "r")

            axis("equal")
            title("Prism Cross Disperser")
            if export_graph:
                savefig("spectral_format.png",dpi=150)
            #show()


            # Resolution
            figure(figsize=(6,4))
            res3 = []
            res2_5 = []
            lo = []
            lom= []
            loM = []
            for i in range(int(self.order_min.value),int(self.order_max.value)+1):
                lo.append(2*sin(self.Bldeg.value*pi/180)/(i*self.opt_d.value)*1000000)
                dl = lo[-1] / i
                dbLoc = 2 * sin(self.Bldeg.value*pi/180)/(i*cos(self.Bldeg.value*pi/180))
                res3.append(lo[-1]/(dl/(dbLoc*self.f_c.value / ( 3 * self.pix_size.value*1e-3))))
                res2_5.append(lo[-1]/(dl/(dbLoc*self.f_c.value / ( 2.5 * self.pix_size.value*1e-3))))
            plot(lo,res3)
            plot(lo,res2_5)
            legend(("3 pixels sampling","2.5 pixels sampling"))
            xlim(self.elmin.value,self.elmax.value)
            title("Zero order resolution of spectrograph")
            if export_graph:
                savefig("resolution.png",dpi=150)
            show()
    
    def export_spectro(self,b):
        with self.out:
            ##print("coucou")
            ##print(self.__dict__)
            workbook = xlsxwriter.Workbook('spectro'+time.strftime("%Y%m%d-%H%M%S",time.localtime())+'.xlsx')
            worksheet = workbook.add_worksheet("spectro")
            worksheet_order = workbook.add_worksheet("orders")
            Sbold = workbook.add_format({'bold': True,'border':True,'bg_color':"#01BAEF"})
            sTitle = workbook.add_format({'bold': True,'border':True,'bg_color':'#0B4F6C','font_color':'white'})
            sTitle0 = workbook.add_format({'bold': True,'border':True,'bg_color':'#20BF55'})
            Snumbers = workbook.add_format({'border':True,'bg_color':"#FBFBFF",'align':'left'})

            SoddO = workbook.add_format({'border': True, 'bg_color': "#01BAEF", 'align': 'left'})
            SevenO = workbook.add_format({'border': True, 'bg_color': "#AD00F0", 'align': 'left'})

            worksheet.write('B1',self.Title.value,sTitle0)
            worksheet.write('B2','',sTitle0)
            worksheet.write('B3','',sTitle)
            
            worksheet.write('B4',self.Nfiber.value,Snumbers)
            worksheet.write('B5',self.NA_fiber.value,Snumbers)
            
            worksheet.write('B6','',sTitle)
            
            worksheet.write('B7',self.lmin.value,Snumbers) 
            worksheet.write('B8',self.lmax.value,Snumbers)
            worksheet.write('B9',self.Res.value,Snumbers)
            
            worksheet.write('B10','',sTitle)
            
            worksheet.write('B11',self.Bldeg.value,Snumbers)
            if self.use_real_grating.value:
                worksheet.write('B12',self.real_d.value,Snumbers)
            worksheet.write('B13',"",Snumbers)
            worksheet.write('B14',"",Snumbers)
            
            worksheet.write('B15','',sTitle)
            
            worksheet.write('B16',self.spl_PSF.value,Snumbers)
            worksheet.write('B17',self.pix_size.value,Snumbers)
            worksheet.write('B18',self.nx.value,Snumbers)
            worksheet.write('B19',self.InterFiber.value,Snumbers)
            worksheet.write('B20',self.InterOrder.value,Snumbers)
            worksheet.write('B21',self.orderCrossThickness.value,Snumbers)
            worksheet.write('B22',self.OrderOverlap.value,Snumbers)
            worksheet.write('B23',self.MarginLargest_order.value,Snumbers)
            
            worksheet.write('B24','',sTitle0)
            worksheet.write('B25','',sTitle)
            
            worksheet.write('B26',self.opt_d.value,Snumbers)
            worksheet.write('B27',self.g_length.value,Snumbers)
            worksheet.write('B28',self.g_height.value,Snumbers)
            if self.use_real_grating.value:
                worksheet.write('B29',self.max_length.value,Snumbers)
                worksheet.write('B30',self.max_height.value,Snumbers)
                worksheet.write('B31',self.Reference.value,Snumbers)
                worksheet.write('B32',self.Manufacturer.value,Snumbers)
            
            worksheet.write('B33','',sTitle)
            worksheet.write('B34','',sTitle)
            
            worksheet.write('B35',self.prism_thickness.value,Snumbers)
            worksheet.write('B36',self.prism_margin.value,Snumbers)
            worksheet.write('B37',self.prism_apex.value,Snumbers)
            worksheet.write('B38',self.prism_input_angle.value,Snumbers)
            
            worksheet.write('B39','',sTitle)
            
            worksheet.write('B40',self.cd_Opt_d.value,Snumbers)
            worksheet.write('B41',self.bet.value,Snumbers)
            worksheet.write('B42',self.m_p.value,Snumbers)
            
            worksheet.write('B43','',sTitle)
            
            worksheet.write('B44',self.f_c.value,Snumbers)
            worksheet.write('B45',self.cameraFN.value,Snumbers)
            worksheet.write('B46',self.Field_Camera.value,Snumbers)
            
            worksheet.write('B47','',sTitle)
            
            worksheet.write('B48',self.ActualSampling.value,Snumbers)
            worksheet.write('B49',self.elmin.value,Snumbers)
            worksheet.write('B50',self.elmax.value,Snumbers)
            worksheet.write('B51',self.No.value,Snumbers)
            worksheet.write('B52',self.order_min.value,Snumbers)
            worksheet.write('B53',self.order_max.value,Snumbers)
            
            worksheet.write('B54','',sTitle)
            
            worksheet.write('B55',self.Fiber_distance.value,Snumbers)
            worksheet.write('B56',self.Mean_order_tilt.value,Snumbers)
            
            
            ####################
            
            
            worksheet.write('A1','Title',sTitle0)
            worksheet.write('A2','Input Parameter',sTitle0)
            worksheet.write('A3','Fiber Parameters',sTitle)
            
            worksheet.write('A4','Fiber Number',Sbold)
            worksheet.write('A5','Fiber NA',Sbold)
            
            worksheet.write('A6','Spectrum Parameters',sTitle)
            
            worksheet.write('A7','Lambda min',Sbold) 
            worksheet.write('A8','Lambda max',Sbold)
            worksheet.write('A9','Resolution',Sbold)
            
            worksheet.write('A10','Grating Parameters',sTitle)
            
            worksheet.write('A11','Grating Blaze angle',Sbold)
            if self.use_real_grating.value:
                worksheet.write('A12','Off the shelf Grating Density',Sbold)
            else:
                worksheet.write('A12','',Sbold)
            worksheet.write('A13','',Sbold)
            worksheet.write('A14',"",Sbold)
            
            worksheet.write('A15','CCD parameters',sTitle)
            
            worksheet.write('A16','Pixel Sampling Dispertion direction',Sbold)
            worksheet.write('A17','CCD pixels Size',Sbold)
            worksheet.write('A18','Number of pixel of the CCD',Sbold)
            worksheet.write('A19','Distance in pixel between the different fiber spectrum',Sbold)
            worksheet.write('A20','Additional distance between orders',Sbold)
            worksheet.write('A21','Sampling ot the spectrum in the cross dispersion direction',Sbold)
            worksheet.write('A22','Overlap of the order in %',Sbold)
            worksheet.write('A23','Margin on the border of the CCD in %',Sbold)
            
            
            
            worksheet.write('A24','Output Parameters',sTitle0)
            worksheet.write('A25','Grating',sTitle)
            
            worksheet.write('A26','Grating density',Sbold)
            worksheet.write('A27','Required length of the grating',Sbold)
            worksheet.write('A28','Required height of the grating',Sbold)
            if self.use_real_grating.value:
                worksheet.write('A29','length of the mother Grating',Sbold)
                worksheet.write('A30','Height of the mother Grating',Sbold)
                worksheet.write('A31','Reference of the Grating',Sbold)
                worksheet.write('A32','Manufacturer',Sbold)
            else:
                worksheet.write('A29','',Sbold)
                worksheet.write('A30','',Sbold)
                worksheet.write('A31','',Sbold)
                worksheet.write('A32','',Sbold)
            
            worksheet.write('A33','Cross Disperser',sTitle)
            worksheet.write('A34','Case of the prism',sTitle)
            
            worksheet.write('A35','Prism Thickness',Sbold)
            worksheet.write('A36','Prism Margin',Sbold)
            worksheet.write('A37','Prism Apex',Sbold)
            worksheet.write('A38','Prism Imput Angle',Sbold)

            worksheet.write('A39','Case of the Grating',sTitle)
            
            worksheet.write('A40','Cross Disperer Grating Period',Sbold)
            worksheet.write('A41','Cross Disperser Output Angle',Sbold)
            worksheet.write('A42','Cross Disperer Density',Sbold)

            worksheet.write('A43','Camera Parameters',sTitle)
            
            worksheet.write('A44','Camera Focal Length',Sbold)
            worksheet.write('A45','Camera F Number',Sbold)
            worksheet.write('A46','Camera Field Of View',Sbold)

            worksheet.write('A47','Spectrum Caracteristics',sTitle)
            
            worksheet.write('A48','Actual Sampling in the dispersion direction in pixels',Sbold)
            worksheet.write('A49','Effective Lambda min',Sbold)
            worksheet.write('A50','Effective Lambda Max',Sbold)
            worksheet.write('A51','Number of order',Sbold)
            worksheet.write('A52','Minimum Order',Sbold)
            worksheet.write('A53','Maximum Order',Sbold)
            
            worksheet.write('A54','Miscelaneous',sTitle)
            
            worksheet.write('A55','Distance of the fiber in the focal plane in microns',Sbold)
            worksheet.write('A56','Mean Order Tilt',Sbold)

            #worksheet.write('B43',self.real_d.options.value)
            worksheet.set_column("A:A",60)
            worksheet.set_column("B:B",20)
            
            self.compute_spectro(b,export_graph=True)
            
            worksheet.insert_image('C1', 'efficiency.png')
            worksheet.insert_image('C20', 'efficiency_2.png')
            worksheet.insert_image('C40', 'spectral_format.png')
            worksheet.insert_image('C70', 'resolution.png')
            



            worksheet_order.write ("A1","Spaxel",sTitle)
            worksheet_order.write("B1", "Order Number", sTitle)
            worksheet_order.write("C1", "Lmin", sTitle)
            worksheet_order.write("D1", "Lmax", sTitle)
            worksheet_order.write("E1", "pix min offset", sTitle)
            worksheet_order.write("F1", "pix max offset", sTitle)
            worksheet_order.write("G1", "pix min", sTitle)
            worksheet_order.write("H1", "pix max", sTitle)

            offset_fiber_tilt = self.Nfiber.value * self.OuterDiameterFiber.value * cos(
                self.FiberAngle.value * pi / 180) / self.Fiber_distance.value * self.InterFiber.value

            i = self.order_min.value

            loc = 2 * sin(self.Bldeg.value * pi / 180) / (i * self.opt_d.value) * 1000000
            dl = loc / i * self.c_m
            ll = linspace(loc - dl, loc + dl, 10000)

            # #print(i,loc-dl,loc+dl)
            beth = arcsin(i * ll * 1e-6 * self.opt_d.value - sin(self.Bldeg.value * pi / 180))
            beth_c = arcsin(i * loc * 1e-6 * self.opt_d.value - sin(self.Bldeg.value * pi / 180))
            pix = (beth - beth_c) * self.f_c.value * 1000 / self.pix_size.value + self.nx.value / 2


            pmin = pix.min()

            pmax = pix.max()

            off = (self.nx.value /2 - (pmin+pmax)/2)
            #print("offsets : ",pmin,pmax,off)



            for i in range (int(self.order_min.value),int(self.order_max.value+1)):
                if i%2:
                    st = SevenO
                else:
                    st = SoddO

                for j in range(int(self.Nfiber.value)):
                    worksheet_order.write(f"A{(i-self.order_min.value)*self.Nfiber.value+2+j}",j+1,st)
                    worksheet_order.write(f"B{(i - self.order_min.value) * self.Nfiber.value + 2 + j}", i,st)
                loc = 2 * sin(self.Bldeg.value * pi / 180) / (i * self.opt_d.value) * 1000000
                dl = loc / i * self.c_m
                ll = linspace(loc - dl, loc + dl, 10000)


                # #print(i,loc-dl,loc+dl)
                beth = arcsin(i * ll * 1e-6 * self.opt_d.value - sin(self.Bldeg.value * pi / 180))
                beth_c = arcsin(i * loc * 1e-6 * self.opt_d.value - sin(self.Bldeg.value * pi / 180))
                pix = (beth - beth_c) * self.f_c.value * 1000 / self.pix_size.value + self.nx.value / 2
                for j in range(int(self.Nfiber.value)):
                    #eff_g0[(pix_g < 0 - offset_fiber_tilt / 2) | (pix_g > self.nx.value - offset_fiber_tilt / 2)] = 0
                    #eff_g0d[(pix_g < 0 + offset_fiber_tilt / 2) | (pix_g > self.nx.value + offset_fiber_tilt / 2)] = 0
                    mm = where((pix > (0 - offset_fiber_tilt / 2 - off + offset_fiber_tilt/(self.Nfiber.value-1) * j)) & (pix < (self.nx.value - offset_fiber_tilt / 2 - off +  offset_fiber_tilt/(self.Nfiber.value-1) * j)))
                    lmin = ll[mm].min()
                    lmax = ll[mm].max()
                    worksheet_order.write(f"C{(i - self.order_min.value) * self.Nfiber.value + 2 + j}", lmin, st)
                    worksheet_order.write(f"D{(i - self.order_min.value) * self.Nfiber.value + 2 + j}", lmax, st)
                    worksheet_order.write(f"E{(i - self.order_min.value) * self.Nfiber.value + 2 + j}", min(pix + off +offset_fiber_tilt / 2 -  offset_fiber_tilt/(self.Nfiber.value-1) * j), st)
                    worksheet_order.write(f"F{(i - self.order_min.value) * self.Nfiber.value + 2 + j}", max(pix + off + offset_fiber_tilt / 2 -  offset_fiber_tilt/(self.Nfiber.value-1) * j), st)
                    worksheet_order.write(f"G{(i - self.order_min.value) * self.Nfiber.value + 2 + j}", min(pix + off ), st)
                    worksheet_order.write(f"H{(i - self.order_min.value) * self.Nfiber.value + 2 + j}", max(pix +off), st)

            workbook.close()
            
    def waves(self,order):
        wl=[]
        for o in order:
            lc = 2*sin(self.Bldeg.value*pi/180)/(o*self.opt_d.value)*1000
            wl.append([lc,
                       lc - self.c_m * lc / o,
                       lc + self.c_m * lc / o])
        return array(wl).T
            
    def export_spectro_zmx(self,b):
            with self.out:
                op = zmxgen.optical_model()
                f = 1e-3*c_[(arange(self.Nfiber.value)-floor(self.Nfiber.value/2))*self.Fiber_distance.value/tan(self.FiberAngle.value*pi/180),
                                    (arange(self.Nfiber.value)-floor(self.Nfiber.value/2))*self.Fiber_distance.value]
                f = self.CollFN.value/(0.5/ self.NA_fiber.value)*f
                op.fields(fields=f)
                #print(c_[zeros(int(self.Nfiber.value)),
                #                    (arange(self.Nfiber.value)-self.Nfiber.value/2)*self.Fiber_distance.value])
                ocenter = floor((self.order_min.value + self.order_max.value)/2)
                
                orders = array([self.order_min.value,self.order_min.value+1,
                                ocenter,ocenter+1,self.order_max.value-1,self.order_max.value])
                
                wls=self.waves(orders)
                #print(wls)
                op.wavelength_flat(wavelength=wls[:,2])
                op.nconfigs(6)
                op.wave_config(wavelengths=wls.T)

                op.NA_aperture(0.5/self.CollFN.value,telecentric=True)
                
                
                
                fcol = floor((self.g_height.value*self.CollFN.value/10)+1)*10
                #print(fcol)
                
                
                s0=zmxgen.surface(0,0,0)
                if self.exportZMXCho.value == 1:
                    # HAPRS like model
                    op.param_config(9, 2, -orders)
                    thetacol = 10
                    dycol = fcol*2/(1+cos(thetacol*pi/180))*sin(thetacol*pi/180)
                    fs = fcol*2/(1+cos(thetacol*pi/180))
                    s1=zmxgen.coordbreak(decentery=dycol,tiltx=10,order = 1)

                    s2=zmxgen.surface(0,0,fcol)
                    s3=zmxgen.surface(-1/(2*fcol),-1,0,glass="MIRROR",comment="Collimator",decentery=-dycol,reverse=True)
                    s4=zmxgen.surface(0,0,-fcol)
                    s5=zmxgen.surface(0,0,0)

                    s6=zmxgen.coordbreak(tilty=self.Bldeg.value)
                    s7=zmxgen.coordbreak(tiltx=2)
                    s8=zmxgen.coordbreak(tiltz=-90)
                    s9=zmxgen.surface(0,0,0,glass="MIRROR",stop=True,comment="Echelle")
                    s10=zmxgen.coordbreak(tiltz="P8 -1 0")
                    s11=zmxgen.coordbreak(tiltx="P7 -1 0")
                    s12=zmxgen.coordbreak(tilty="P6 -1 0")
                    s13=zmxgen.surface(0,0,fcol)
                    s14=zmxgen.surface(-1/(2*fcol),-1,0,glass="MIRROR",comment="Collimator",decentery=-dycol,reverse=True)
                    s15=zmxgen.coordbreak(decentery=-dycol,order =1)
                    s16=zmxgen.surface(0,0,-fcol,decentery=dycol)
                    s17=zmxgen.surface(0,0,0)
                    s18=zmxgen.surface(0,0,"P16 -1 0",glass="MIRROR",comment="Focal mirror",decentery=-dycol)
                    s19=zmxgen.surface(-1/(2*fcol),-1,0,glass="MIRROR",comment="Collimator")
                    s20=zmxgen.coordbreak(decenterx="P-1 1 0",
                                          decentery="P-1 1 0",
                                          tiltx="P-1 1 0")
                    s21=zmxgen.coordbreak(tilty=0)
                    s22=zmxgen.coordbreak(tiltx=-self.prism_input_angle.value)
                    s23=zmxgen.surface(0,0,-70,glass="SF11",comment="CD prism")
                    s24=zmxgen.coordbreak(decenterx="P-1 1 0",decentery="P-1 1 0",tiltx=self.prism_apex.value)
                    s25=zmxgen.surface(0,0,0,comment="CD prism")
                    s26=zmxgen.coordbreak(tiltx="P-1 1 0")
                    s27=zmxgen.surface(0,0,-200)
                    s29=zmxgen.surface(0,0,0)



                    op.standard_surface(s0)
                    op.coordbreak_surface(0,s1)
                    op.standard_surface(s2)
                    op.standard_surface(s3)
                    op.standard_surface(s4)
                    op.standard_surface(s5)
                    op.coordbreak_surface(0,s6)
                    op.coordbreak_surface(0,s7)
                    op.coordbreak_surface(0,s8)
                    op.grating_surface(s9,self.opt_d.value/1000,-orders[0])
                    op.coordbreak_surface(0,s10)
                    op.coordbreak_surface(0,s11)
                    op.coordbreak_surface(0,s12)
                    op.standard_surface(s13)
                    op.standard_surface(s14)
                    op.coordbreak_surface(0,s15)
                    op.standard_surface(s16)
                    op.standard_surface(s17)
                    op.standard_surface(s18)
                    op.standard_surface(s19)
                    op.coordbreak_surface(-fcol,s20)
                    op.coordbreak_surface(0,s21)
                    op.coordbreak_surface(0,s22)
                    op.standard_surface(s23)
                    op.coordbreak_surface(0,s24)
                    op.standard_surface(s25)
                    op.coordbreak_surface(0,s26)
                    op.standard_surface(s27)
                    op.paraxial_surface("M1",self.f_c.value)
                    op.standard_surface(s29)
                    op.save('spectro_h_'+time.strftime("%Y%m%d-%H%M%S",time.localtime())+'.zmx')

                if self.exportZMXCho.value == 2:
                    # simple pass paraxial model
                    op.param_config(7, 2, orders)
                    s1 = zmxgen.surface(0, 0, fcol)
                    #s2 paraxial lens for collimator
                    s3 = zmxgen.surface(0, 0, fcol)
                    s4 = zmxgen.coordbreak(tilty=self.Bldeg.value)
                    s5 = zmxgen.coordbreak(tiltx=2)
                    s6 = zmxgen.coordbreak(tiltz=-90)
                    s7 = zmxgen.surface(0, 0, 0, glass="MIRROR", stop=True, comment="Echelle")
                    s8 = zmxgen.coordbreak(tiltz="P6 -1 0")
                    s9 = zmxgen.coordbreak(tiltx="P5 -1 0")
                    s10 = zmxgen.coordbreak(tilty="P4 -1 0")
                    s105 = zmxgen.surface(0, 0, -fcol)
                    s11 = zmxgen.coordbreak(tilty=0)
                    s12 = zmxgen.coordbreak(tiltx=-self.prism_input_angle.value)
                    s13 = zmxgen.surface(0, 0, -70, glass="SF11", comment="CD prism")
                    s14 = zmxgen.coordbreak(decenterx="P-1 1 0", decentery="P-1 1 0", tiltx=self.prism_apex.value)
                    s15 = zmxgen.surface(0, 0, 0, comment="CD prism")
                    s16 = zmxgen.coordbreak(tiltx="P-1 1 0")
                    s165 = zmxgen.surface(0, 0, -fcol)
                    #s17 parxial lens for camera
                    s18 = zmxgen.surface(0, 0, 0)


                    op.standard_surface(s0)
                    op.standard_surface(s1)
                    op.paraxial_surface("M1", fcol)
                    op.standard_surface(s3)
                    op.coordbreak_surface(0, s4)
                    op.coordbreak_surface(0, s5)
                    op.coordbreak_surface(0, s6)
                    op.grating_surface(s7, self.opt_d.value / 1000, orders[0])
                    op.coordbreak_surface(0, s8)
                    op.coordbreak_surface(0, s9)
                    op.coordbreak_surface(0, s10)
                    op.standard_surface(s105)
                    op.coordbreak_surface(0, s11)
                    op.coordbreak_surface(0, s12)
                    op.standard_surface(s13)
                    op.coordbreak_surface(0, s14)
                    op.standard_surface(s15)
                    op.coordbreak_surface(0, s16)
                    op.standard_surface(s165)
                    op.paraxial_surface("M1", self.f_c.value)
                    op.standard_surface(s18)

                    op.save('spectro_ps_' + time.strftime("%Y%m%d-%H%M%S", time.localtime()) + '.zmx')

                if self.exportZMXCho.value == 3:
                    # double pass paraxial model
                    op.param_config(13, 2, orders)
                    s1 = zmxgen.surface(0, 0, fcol)
                    # s2 paraxial lens for collimator
                    s3 = zmxgen.surface(0, 0, fcol)

                    s4 = zmxgen.coordbreak()
                    s5 = zmxgen.surface(0, 0, 70, glass="SF11", comment="CD prism",ytangent=tan(self.inp_a_dbl*pi/180))
                    s6 = zmxgen.coordbreak(decenterx="P-1 1 0", decentery="P-1 1 0")
                    s7 = zmxgen.surface(0, 0, 0, comment="CD prism",ytangent=tan(-(self.apex_dbl-self.inp_a_dbl)*pi/180))
                    s8 = zmxgen.coordbreak(tiltx="P-1 1 0")
                    s85 = zmxgen.surface(0, 0, fcol)

                    s9 = zmxgen.coordbreak(tilty=self.Bldeg.value)
                    s10 = zmxgen.coordbreak(tiltx=2)
                    s11 = zmxgen.coordbreak(tiltz=-90)
                    s12 = zmxgen.surface(0, 0, 0, glass="MIRROR", stop=True, comment="Echelle")
                    s13 = zmxgen.coordbreak(tiltz="P12 -1 0")
                    s14 = zmxgen.coordbreak(tiltx="P11 -1 0")
                    s15 = zmxgen.coordbreak(tilty="P10 -1 0")
                    s16 = zmxgen.surface(0, 0, -fcol)
                    s17 = zmxgen.coordbreak(tilty=0)

                    s18 = zmxgen.coordbreak(tiltx="P8 -1 0")
                    s19 = zmxgen.surface(0, 0, -70, glass="SF11", comment="CD prism",ytangent ="P7 -1 0" )
                    s20 = zmxgen.coordbreak(decenterx="P6 -1 0", decentery="P6 -1 0")
                    s21 = zmxgen.surface(0, 0, 0, comment="CD prism",ytangent = "P5 -1 0")
                    s22 = zmxgen.coordbreak(tiltx="P4 -1 0")

                    s23 = zmxgen.surface(0, 0, -fcol)
                    # s24 parxial lens for camera
                    s25 = zmxgen.surface(0, 0, 0)

                    op.standard_surface(s0)
                    op.standard_surface(s1)
                    op.paraxial_surface("M", fcol)

                    op.standard_surface(s3)
                    op.coordbreak_surface(0, s4)
                    op.tilted_surface(s5)
                    op.coordbreak_surface(0, s6)
                    op.tilted_surface(s7)
                    op.coordbreak_surface(0, s8)
                    op.standard_surface(s85)

                    op.coordbreak_surface(0, s9)
                    op.coordbreak_surface(0, s10)
                    op.coordbreak_surface(0, s11)
                    op.grating_surface(s12, self.opt_d.value / 1000, orders[0])
                    op.coordbreak_surface(0, s13)
                    op.coordbreak_surface(0, s14)
                    op.coordbreak_surface(0, s15)
                    op.standard_surface(s16)
                    op.coordbreak_surface(0, s17)


                    op.coordbreak_surface(0, s18)
                    op.tilted_surface(s19)
                    op.coordbreak_surface(0, s20)
                    op.tilted_surface(s21)
                    op.coordbreak_surface(0, s22)



                    op.standard_surface(s23)
                    op.paraxial_surface("M0", self.f_c.value)
                    op.standard_surface(s25)

                op.save('spectro_pd_' + time.strftime("%Y%m%d-%H%M%S", time.localtime()) + '.zmx')



    def change_available_d(self,b):
        self.out.clear_output()
        with self.out:
            self.real_d.options = gratings["densite"][gratings["blaze"] == self.Bldeg.value]
            self.compute_spectro(b)

    
    def update_wave(self,b):
        with self.out:
            self.licen.value = 2*sin(self.Bldeg.value*pi/180)/(self.Order.value*self.opt_d.value)*1000000
            self.limin.value = self.licen.value - self.c_m * self.licen.value / self.Order.value
            self.limax.value = self.licen.value + self.c_m * self.licen.value / self.Order.value
            #self.compute_spectro(b)
                  
    def tune_resolution(self,b):
        with self.out:
            self.compute_spectro(b)
